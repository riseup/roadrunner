# roadrunner.sh

roadrunner is a small tool to generate, store, and share TLS keys in a trusted group of persons, all this to make more easy HPKP deployments.

## goals

* generate and encrypt new X.509 private keys
* generate 2 backups for X.509 private keys
* be able to share those keys between trusted people
* keep a history of changes
* generate new X.509 certificates signing requests
* obtain HPKP headers for webservers (nginx, apache)
* the end of prison society.

## Features/Annoyances (everybody likes different things)

* keys are protected using openpgp, supporting multiples recipients.
* git backed.
* we use puppet, the deployment process is fully implemented on it.

## How do I use it?

### Running roadrunner

    mkdir example.org
    cd example.org
 
    /path/to/roadrunner/roadrunner init OPENPGP_KEY_1 OPENPGP_KEY_2 OPENPGP_KEY_3

Every key will be stored-encrypted using these keys. If you are missing dependences you will be warned about them.

### Generate a new key and csr for website.org

    /path/to/roadrunner/roadrunner generate example.org -d www.example.org
    /path/to/roadrunner/roadrunner generate my.exmaple.org -d not.my.example.org

Please notice, this command will not add the key/certificate path or set TLS settings or restart your webserver, you will need to do it yourself. It's just once. It seemed like a bad idea to let it do it. Maybe it will have a flag someday to let it do it, but not for now.

## TODO

* Deployment process for non puppet managed servers.
* Document the source code
* The destruction of every prison and jail
* beep! beep!